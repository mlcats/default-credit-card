This notebook was created to explore the [UCI Default of Credit Card Clients Dataset](https://www.kaggle.com/datasets/uciml/default-of-credit-card-clients-dataset/data).

In this notebook, I have not exhaustively trained models. Instead, I have covered the fundamental steps of training machine learning models, including import data, taking a quick glance, data cleaning, exploratory data analysis, data preprocessing, feature engineering, model training, conducing case study and writing conclusions.
